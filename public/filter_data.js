function GetEnergyforSpesificDay(data, date) {
    let peak_power = [];
    let accumulated_energy = [];
    let time_axis = [];
    let filter_date = new Date(date);
    for (i = 1; i < data.rows.length; i++) {
        //Clear dataset
        if (data.rows[i].c[2].v != null) {
            sheet_date = new Date(data.rows[i].c[0].v)
            if( sheet_date.getDate() === filter_date.getDate() && sheet_date.getMonth() === filter_date.getMonth()) {
                peak_power.push(data.rows[i].c[2].v);
                accumulated_energy.push(data.rows[i].c[3].v);
                current_hour = data.rows[i].c[1].v
                time_axis.push(current_hour.substring(0,2))
            }
        }
    }

    return [peak_power, accumulated_energy, time_axis]
}

function GetEnergyforMonth(data, date) {
    let accumulated_energy = []
    let date_axis = [];
    let filter_date = new Date(date);
    accumulated_energy[0] = filter_date.getMonth()
    //date_axis[0] = filter_date.getMonth()
    for (let i = 1; i < data.rows.length; i++) {
        if (data.rows[i].c[2].v != null) {
            sheet_date = new Date(data.rows[i].c[0].v)
            if( sheet_date.getMonth() === filter_date.getMonth()) {

                energy = (data.rows[i].c[3].v)
                //console.log(energy,sheet_date.getDate())
                if(energy > accumulated_energy[sheet_date.getDate()] || accumulated_energy.length < sheet_date.getDate()+1) {
                    accumulated_energy[sheet_date.getDate()] = energy;
                    current_date = data.rows[i].c[4].v
                    //console.log(current_date)
                    date_axis[sheet_date.getDate()] = sheet_date.getDate()
                }

            }
        }
    }
    return [accumulated_energy, date_axis]
    //console.log(date_axis)
    //console.log(accumulated_energy)
}

function GetTextFromMonth(month_nr) {
    let  months = ["January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"];
    return months[month_nr]
}